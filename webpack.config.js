const path = require('path')
const webpack = require('webpack')

module.exports = {
    mode: 'development',
    entry: {
        main: [
            '/VT.js'
        ]
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ]
}