var userlogged=localStorage?.getItem("userlogged");
var user=(userlogged === ""? null : JSON.parse(userlogged));
const baseURL = "https://hpusmart.info/api/";
const functioner ='?r=services/login-web';
const timestamp = Math.floor(Date.now() / 1000);
var url = baseURL+functioner+"&timestamp="+timestamp.toString();
$('#button_loggin').click(function(event){
    event.preventDefault();
    $.ajax({
      url: url,
      data:$(".form_loggin").serializeArray(),
      dataType:"json",
      type:"post",
      beforeSend: function(){
      },
      success: function(dota){
        localStorage.setItem("userlogged",JSON.stringify(dota.user));
        window.location.href = "./page/main_page.html";
        alertify.success(dota.message)
      },
      error: function(hello){
          alertify.error(hello.responseJSON.message)
      }
      
    })
})

